package xyz.symbx.audio;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.util.Log;

import java.util.List;

public class AmpCommand {
    private final BluetoothGattCallback _callback = new BluetoothGattCallback() {
        @SuppressLint("MissingPermission")
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            // Log.d("SYMBX", String.format("Connect %d -> %d", status, newState));
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    // Log.d("SymbX", "Connected");
                    gatt.discoverServices();
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    // Log.d("SymbX", "Disconnected");
                    gatt.close();
                } else {
                    // Log.d("SymbX", "Another state?");
                }
            } else {
                // Log.d("SymbX", "Fail 1");
                gatt.close();
            }
        }

        @SuppressLint("MissingPermission")
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            List<BluetoothGattService> services = gatt.getServices();
            for (BluetoothGattService service : services) {
                if (!service.getUuid().toString().equals(Constants.SERVICE_UUID)) {
                    // Log.d("SYMBX", "SRV" + service.getUuid().toString());
                    continue;
                }
                // Log.d("SymbX", "Service: " + service.getUuid().toString());
                for (BluetoothGattCharacteristic chr : service.getCharacteristics()) {
                    if (!chr.getUuid().toString().equals(Constants.CHAR_UUID)) {
                        // Log.d("SYMBX", "Char" + chr.getUuid().toString());
                        continue;
                    }
                    // Log.d("SymbX", "Char: " + chr.getUuid().toString());
                    int props = chr.getProperties();
                    if ((props & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                        gatt.setCharacteristicNotification(chr, true);
                        chr.setValue(_packet);
                        gatt.writeCharacteristic(chr);
                    }
                }
            }
            gatt.disconnect();
            gatt.close();
        }
    };
    private byte[] _packet = null;
    private BluetoothDevice _device = null;

    protected AmpCommand(BluetoothDevice device) {
        _device = device;
    }

    protected void init(int len) {
        _packet = new byte[len];
        _packet[0] = (byte) 0xAA; // Header
    }

    public static void sendVolume(BluetoothDevice dev, byte vol) {
        AmpCommand cmd = new AmpCommand(dev);
        cmd.init(3);
        cmd._packet[1] = 0x15;
        cmd._packet[2] = vol;
        cmd.send();
    }

    public static void sendTone(BluetoothDevice dev, byte bass, byte middle, byte treble) {
        AmpCommand cmd = new AmpCommand(dev);
        cmd.init(5);
        cmd._packet[1] = 0x16;
        cmd._packet[2] = (byte) (bass + 14);
        cmd._packet[3] = (byte) (middle + 14);
        cmd._packet[4] = (byte) (treble + 14);
        cmd.send();
    }

    @SuppressLint("MissingPermission")
    public void send() {
        _device.connectGatt(null, false, _callback);
    }
}
