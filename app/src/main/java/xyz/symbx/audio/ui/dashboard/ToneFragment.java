package xyz.symbx.audio.ui.dashboard;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.sdsmdg.harjot.crollerTest.Croller;
import com.sdsmdg.harjot.crollerTest.OnCrollerChangeListener;

import xyz.symbx.audio.databinding.FragmentToneBinding;
import xyz.symbx.audio.ui.home.AmpViewModel;

public class ToneFragment extends Fragment {

    private AmpViewModel model;
    private FragmentToneBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentToneBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        model = new ViewModelProvider(requireActivity()).get(AmpViewModel.class);
        if (!model.setupViewModel(this.getActivity())) {
            requireActivity().finish();
            return root;
        }
        model.refreshPairedDevices();

        binding.crollerBass.setOnCrollerChangeListener(new OnCrollerChangeListener() {
            @Override
            public void onProgressChanged(Croller croller, int progress) {
                Log.d("SYMBX", String.format("[b] ctrl->model %d (%d)", progress, progress * 2 - 14));
                model.setBass(progress * 2 - 14);
            }

            @Override
            public void onStartTrackingTouch(Croller croller) {

            }

            @Override
            public void onStopTrackingTouch(Croller croller) {

            }
        });
        model.getBass().observe(getViewLifecycleOwner(), (progress) -> {
            Log.d("SYMBX", String.format("[b] model->ctrl %d (%d)", progress, progress / 2 + 7));
            binding.crollerBass.setProgress(progress / 2 + 7);
        });
        binding.crollerMiddle.setOnCrollerChangeListener(new OnCrollerChangeListener() {
            @Override
            public void onProgressChanged(Croller croller, int progress) {
                Log.d("SYMBX", String.format("[m] ctrl->model %d (%d)", progress, progress * 2 - 14));
                model.setMiddle(progress * 2 - 14);
            }

            @Override
            public void onStartTrackingTouch(Croller croller) {

            }

            @Override
            public void onStopTrackingTouch(Croller croller) {

            }
        });
        model.getMiddle().observe(getViewLifecycleOwner(), (progress) -> {
            Log.d("SYMBX", String.format("[m] model->ctrl %d (%d)", progress, progress / 2 + 7));
            binding.crollerMiddle.setProgress(progress / 2 + 7);
        });
        binding.crollerTreble.setOnCrollerChangeListener(new OnCrollerChangeListener() {
            @Override
            public void onProgressChanged(Croller croller, int progress) {
                Log.d("SYMBX", String.format("[t] ctrl->model %d (%d)", progress, progress * 2 - 14));
                model.setTreble(progress * 2 - 14);
            }

            @Override
            public void onStartTrackingTouch(Croller croller) {

            }

            @Override
            public void onStopTrackingTouch(Croller croller) {

            }
        });
        model.getTreble().observe(getViewLifecycleOwner(), (progress) -> {
            Log.d("SYMBX", String.format("[t] model->ctrl %d (%d)", progress, progress / 2 + 7));
            binding.crollerTreble.setProgress(progress / 2 + 7);
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}