package xyz.symbx.audio.ui.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import xyz.symbx.audio.AmpCommand;
import xyz.symbx.audio.R;

public class AmpViewModel extends ViewModel {

    private BluetoothAdapter _adapter;
    private final List<BluetoothDevice> _devices = new ArrayList<>();
    private boolean _setup = false;
    private boolean _scanning = false;
    private ScanSettings _settings;
    private ScanFilter _filter;
    private final SavedStateHandle _state;

    private final MutableLiveData<Collection<BluetoothDevice>> _devicesData = new MutableLiveData<>();
    private final MutableLiveData<BluetoothDevice> _device = new MutableLiveData<>(null);
    private final MutableLiveData<Integer> _volume;
    private final MutableLiveData<Integer> _bass;
    private final MutableLiveData<Integer> _middle;
    private final MutableLiveData<Integer> _treble;

    private final ScanCallback _callback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            BluetoothDevice dev = result.getDevice();
            for (BluetoothDevice device : _devices) {
                if (device.getAddress().equals(dev.getAddress())) {
                    return;
                }
            }
            _devices.add(dev);
            _devicesData.postValue(_devices);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            _devices.clear();
            for (ScanResult res : results) {
                _devices.add(res.getDevice());
            }
            _devicesData.postValue(_devices);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.d("SymbX", "Scan fail");
        }
    };
    private BluetoothLeScanner _scanner;

    public AmpViewModel(SavedStateHandle state) {
        _state = state;
        _volume = state.getLiveData("volume", 30);
        _bass = state.getLiveData("bass", 0);
        _middle = state.getLiveData("middle", 0);
        _treble = state.getLiveData("treble", 0);
        Log.d("SYMBX", "Model created");
    }

    public LiveData<Integer> getVolume() {
        return _volume;
    }

    public void setVolume(Integer vol) {
        if (_volume.getValue() != vol) {
            _volume.postValue(vol);
            Log.d("SYMBX", String.format("Vol: %d, Bass: %d, Mid: %d, Tre: %d", _volume.getValue(), _bass.getValue(), _middle.getValue(), _treble.getValue()));
            BluetoothDevice dev = _device.getValue();
            if (dev != null) {
                AmpCommand.sendVolume(dev, vol.byteValue());
            } else {
                Log.e("SYMBX", "No device");
            }
        } else {
            Log.e("SYMBX", "Same volume");
        }
    }

    public boolean setupViewModel(Activity activity) {
        if (!_setup) {
            Log.d("SYMBX", "Setup");
            _setup = true;
            _adapter = ((BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
            if (_adapter == null) {
                Toast.makeText(activity.getApplication(), R.string.no_bluetooth, Toast.LENGTH_LONG).show();
                return false;
            }
            _scanner = _adapter.getBluetoothLeScanner();
            _settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            _filter = new ScanFilter.Builder()
                    //.setServiceUuid(ParcelUuid.fromString(Constants.SERVICE_UUID))
                    .build();
        }
        return true;
    }

    @SuppressLint("MissingPermission")
    public void refreshPairedDevices() {
        if (!_scanning) {
            Log.d("SYMBX", "Start scan");
            _scanner.startScan(Collections.singletonList(_filter), _settings, _callback);
            _scanning = true;
        }
    }

    @Override
    protected void onCleared() {
        this.stopScan();
    }

    @SuppressLint("MissingPermission")
    public void stopScan() {
        if (_scanning) {
            Log.d("SYMBX", "Stop scan");
            _scanner.stopScan(_callback);
        }
    }

    public LiveData<Collection<BluetoothDevice>> getPairedDeviceList() {
        return _devicesData;
    }

    public boolean isBluetoothEnabled() {
        if (_adapter == null) {
            return false;
        }
        return _adapter.isEnabled();
    }

    public LiveData<BluetoothDevice> getDevice() {
        return _device;
    }

    public void setDevice(int id) {
        if (id >= 0 && _devices.size() > id && _devices.get(id) != null) {
            _device.postValue(_devices.get(id));
        } else {
            _device.postValue(null);
        }
    }

    public LiveData<Integer> getBass() {
        return _bass;
    }

    public void setBass(Integer bass) {
        if (_bass.getValue() != bass) {
            Log.d("SYMBX", "Bass " + bass);
            _bass.postValue(bass);
            BluetoothDevice dev = _device.getValue();
            if (dev != null) {
                AmpCommand.sendTone(dev, bass.byteValue(), _middle.getValue().byteValue(), _treble.getValue().byteValue());
            } else {
                Log.e("SYMBX", "No device");
            }
        }
    }

    public LiveData<Integer> getMiddle() {
        return _middle;
    }

    public void setMiddle(Integer middle) {
        if (_middle.getValue() != middle) {
            Log.d("SYMBX", "Middle " + middle);
            _middle.postValue(middle);
            BluetoothDevice dev = _device.getValue();
            if (dev != null) {
                AmpCommand.sendTone(dev, _bass.getValue().byteValue(), middle.byteValue(), _treble.getValue().byteValue());
            } else {
                Log.e("SYMBX", "No device");
            }
        }
    }

    public LiveData<Integer> getTreble() {
        return _treble;
    }

    public void setTreble(Integer treble) {
        if (_treble.getValue() != treble) {
            Log.d("SYMBX", "Treble " + treble);
            _treble.postValue(treble);
            BluetoothDevice dev = _device.getValue();
            if (dev != null) {
                AmpCommand.sendTone(dev, _bass.getValue().byteValue(), _middle.getValue().byteValue(), treble.byteValue());
            } else {
                Log.e("SYMBX", "No device");
            }
        }
    }
}