package xyz.symbx.audio.ui.home;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.sdsmdg.harjot.crollerTest.Croller;
import com.sdsmdg.harjot.crollerTest.OnCrollerChangeListener;

import xyz.symbx.audio.R;
import xyz.symbx.audio.databinding.FragmentVolumeBinding;

public class VolumeFragment extends Fragment {

    private AmpViewModel model;
    private FragmentVolumeBinding binding;

    @SuppressLint("MissingPermission")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentVolumeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        model = new ViewModelProvider(requireActivity()).get(AmpViewModel.class);
        if (!model.setupViewModel(this.getActivity())) {
            requireActivity().finish();
            return root;
        }
        model.refreshPairedDevices();

        final Croller croller = binding.croller;
        croller.setOnCrollerChangeListener(new OnCrollerChangeListener() {
            @Override
            public void onProgressChanged(Croller croller, int progress) {
                model.setVolume(progress);
            }

            @Override
            public void onStartTrackingTouch(Croller croller) {

            }

            @Override
            public void onStopTrackingTouch(Croller croller) {

            }
        });
        model.getVolume().observe(getViewLifecycleOwner(), croller::setProgress);
        final Spinner spinner = binding.deviceSpinner;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(), R.layout.device_item);
        spinner.setAdapter(adapter);
        model.getPairedDeviceList().observe(getViewLifecycleOwner(), (devices) -> {
            adapter.clear();
            for (BluetoothDevice device : devices) {
                adapter.add(device.getName());
            }
        });
        model.getDevice().observe(getViewLifecycleOwner(), (device) -> {
            croller.setEnabled(device != null);
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("SYMBX", String.format("Select item %d / %d", i, l));
                model.setDevice(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.d("SYMBX", "None");
                model.setDevice(-1);
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}