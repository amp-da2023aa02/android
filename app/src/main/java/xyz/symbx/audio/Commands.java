package xyz.symbx.audio;

public enum Commands {
    Volume((byte) 0x15);

    private final byte _value;

    Commands(byte value) {
        this._value = value;
    }

    public byte getValue() {
        return _value;
    }
}
